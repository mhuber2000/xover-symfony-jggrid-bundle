<?php

namespace Xover\Symfony\JqGridBundle\Twig;
/**
 * class XoverJqGridExtension
 *
 * @author Markus Huber <mh@jm-data.at>
 * @since Dec 18, 2014 11:37:25 AM
 * 
 * 
 * 
 */
class XoverJqGridExtension 
    extends \Twig_Extension
{
    const DEFAULT_TEMPLATE = 'XoverSymfonyJqGridBundle::blocks.html.twig';
    
    /**
     * @var array
     */
    protected $templates;
    
    private $environment;
    
    private $router;
    
    public function __construct( $router )
    {
        $this->router = $router;
    }
    
    /**
     * Initializes the runtime environment.
     *
     * This is where you can load some file that contains filter functions for instance.
     *
     * @param Twig_Environment $environment The current Twig_Environment instance
     * 
     * 
     * 
     */
    public function initRuntime( \Twig_Environment $environment )
    {
        $this->environment = $environment;
    }
    
    public function getName()
    {
        return 'xover.twig.jqgrid.extension';
    }
    
    public function getFunctions()
    {
        return array(
            'xoverJqGrid' => new \Twig_Function_Method(
                $this, 
                'renderGrid',
                array(
                    'is_safe' => array(
                        'html'
                    )
                )
            )
        );
    }
    
    public function renderGrid( $grid )
    {
        return $this->renderBlock( 'jqgrid_container', array( 'grid' => $grid ) );
    }
    
    /**
     * Render block
     *
     * @param $name string
     * @param $parameters string
     * @return string
     */
    private function renderBlock( $name, $parameters )
    {
        foreach ( $this->getTemplates() as $template ) {
            if ( $template->hasBlock( $name ) ) {
                return $template->renderBlock( $name, $parameters );
            }
        }

        throw new \InvalidArgumentException(sprintf( 'Block "%s" doesn\'t exist in grid template.', $name ) );
    }
    
    private function getTemplates()
    {
        if ( empty( $this->templates ) ) {
            $this->templates[] = $this->environment->loadTemplate( self::DEFAULT_TEMPLATE );
        }

        return $this->templates;
    }
}