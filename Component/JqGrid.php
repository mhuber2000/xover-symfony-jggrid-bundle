<?php

namespace Xover\Symfony\JqGridBundle\Component;

/**
 * class JqGrid
 *
 * @author Markus Huber <mh@jm-data.at>
 * @since Dec 18, 2014 12:22:00 PM
 * 
 * 
 * 
 */
class JqGrid 
{
    /**
     * holds service container
     * @var \Symfony\Component\DependencyInjection\Container
     */
    private $container;

    /**
     * class constructor
     * 
     * @param \Symfony\Component\DependencyInjection\Container $container
     * @return void
     * 
     * 
     * 
     */
    public function __construct( \Symfony\Component\DependencyInjection\Container $container ) 
    {
        $this->container = $container;
    }
}