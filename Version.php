<?php

namespace Xover\Symfony\JqGridBundle;

/**
 * class Version
 *
 * @author Markus Huber <mh@jm-data.at>
 * @since Dec 16, 2014 11:19:18 AM
 * 
 * 
 * 
 */
class Version 
{
    private $version = '0.0.1';

    /**
     * class constructor
     * 
     * @return void
     * 
     * 
     * 
     */
    public function getCurrent() 
    {
        return $this->version;
    }
}