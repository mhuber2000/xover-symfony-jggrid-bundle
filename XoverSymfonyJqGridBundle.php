<?php

namespace Xover\Symfony\JqGridBundle;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Sensio\Bundle\FrameworkExtraBundle\DependencyInjection\Compiler\AddParamConverterPass;

/**
 * class XoverSymfonyJqGridBundle
 *
 * @author Markus Huber <mh@jm-data.at>
 * @since Dec 16, 2014 11:19:18 AM
 * 
 * 
 * 
 */
class XoverSymfonyJqGridBundle 
    extends Bundle
{
    public function build( ContainerBuilder $container )
    {
        parent::build( $container );
    }
}